

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe ContratGUI
 * Définit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau compte dans la table contrat via
 * la saisie des identifiants du Contrat, la date du début du contrat, la date de fin, et le client 
 * aynt le contrat
 *    - Permet l'affichage de tous les Contrats  dans la console
 *    
 * @author Chokote & Leondaris
 * @version 1.3
 * */


public class ContratGUI extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ContratGUI does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ContratGUI
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant Contrat
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour la date du début
	 */
	private JTextField textFieldDateDebut;
	/**
	 * zone de texte pour la date de fin
	 */
	private JTextField textFieldDateFin;
	/**
	 * zone de texte pour le client ayant le contrat
	 */
	private JTextField textFieldClient;

	
		
	private JTextField textFieldRecherche;

	
	
	/**
	 * label identifiant du contrat
	 */
	private JLabel labelIdentifiant;

	/**
	 * label date de debut
	 */
	private JLabel labelDateDebut;
	
	/**
	 * label date de fin
	 */
	private JLabel labelDateFin;
	
	/**
	 * label client ayant un contrat
	 */
	private JLabel labelClient;

	
	private JLabel labelRecherche;
	
	
	

	/**
	 * bouton d'ajout du contrat 
	 */
	private JButton boutonAjouter;
	
	private JButton boutonRecherche;

	/**
	 * bouton qui permet d'afficher tous les contrats 
	 */
	private JButton boutonAffichageTousLesContrats ;

	/**
	 * Zone de texte pour afficher les contrats
	 */
	private JTextArea zoneTextListContrat;
	
	private JTextArea zoneTextContrat;

	/**
	 * Zone de défilement pour la zone de texte
	 */
	private JScrollPane zoneDefilement;

	
	
	/**
	 * instance de ContratDAO permettant les accès à la base de données
	 */
	private ContratDAO monContratDAO;

	/**
	 * Constructeur Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public ContratGUI() {
		// on instancie la classe Contrat DAO
		this.monContratDAO = new ContratDAO ();
		

		// on fixe le titre de la fenêtre
		this.setTitle("Contrat");
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);

		// création du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gérer la position des éléments
		// il autorisera un retaillage de la fenêtre en conservant la
		// présentation
		// BoxLayout permet par exemple de positionner les élements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldIdentifiant = new JTextField();
		textFieldDateDebut = new JTextField();
		textFieldDateFin = new JTextField();
		textFieldClient = new JTextField();
		textFieldRecherche = new JTextField();
		
		boutonAjouter = new JButton("Ajouter");
		boutonRecherche = new JButton ("Rechercher");
		boutonAffichageTousLesContrats = new JButton(
				"Afficher tous les Contrats");
		labelIdentifiant = new JLabel("Identifiant Contrat :");
		labelDateDebut = new JLabel("Date debut :");
		labelDateFin = new JLabel("Date fin :");
		labelClient = new JLabel("Client :");
		labelRecherche = new JLabel ("Rechercher :");

		zoneTextContrat = new JTextArea(10, 20);
		zoneTextContrat.setEditable(false);
		
		zoneTextListContrat = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListContrat);
		zoneTextListContrat.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelIdentifiant);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldIdentifiant);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelDateDebut);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDateDebut);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDateFin);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDateFin);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
	
		containerPanel.add(labelClient);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldClient);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		containerPanel.add(boutonAjouter);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(labelRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(boutonRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(textFieldRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(zoneTextClient);
		

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageTousLesContrats);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des écouteurs sur les boutons pour gérer les évènements
		boutonAjouter.addActionListener(this);
		boutonAffichageTousLesContrats.addActionListener(this);

		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenêtre
		this.setVisible(true);
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO
		ClientDAO clientDAO = new ClientDAO();
		try {
			if (ae.getSource() == boutonAjouter) {
				// on crée l'objet message
				Contrat a = new Contrat(
						Double.parseDouble(this.textFieldIdentifiant.getText()), 
						Date.valueOf(this.textFieldDateDebut.getText()), 
						Date.valueOf(this.textFieldDateFin.getText()), 
						clientDAO.getClient(this.textFieldClient.getText()));
					
					
						
				// on demande à la classe de communication d'envoyer le client
				// dans la table Contrat
				retour = monContratDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutée ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "Contrat ajouté !");
				else
					JOptionPane.showMessageDialog(this, "Erreur ajout compte",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageTousLesContrats) {
				// on demande à la classe ContratDAO d'ajouter le message
				// dans la base de données
				List<Contrat> liste = monContratDAO.getListeContrat();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListContrat.setText("");
				// on affiche dans la console du client les Contrats reçus
				for (Contrat a : liste) {
					zoneTextListContrat.append(a.toString());
					zoneTextListContrat.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			} else if (ae.getSource() == boutonRecherche){
				
				zoneTextContrat.append(monContratDAO.getContrat((this.textFieldRecherche.getText())).toString());
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contrôler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contrôler vos saisies");
		}

	}

	public static void main(String[] args) {
		new ContratGUI ();
	}

}