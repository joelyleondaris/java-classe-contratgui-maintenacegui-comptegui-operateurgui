

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe CompteGUI
 * Définit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau compte dans la table compte via
 * la saisie des identifiants du compte, le fonction du compte,
 *    - Permet l'affichage de tous les compte  dans la console
 *    
 * @author Chokote & Leondaris
 * @version 1.3
 * */


public class CompteGUI extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * CompteGUI
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant compte 
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour le champ fonction
	 */
	private JTextField textFieldFonction;

	
		
	private JTextField textFieldRecherche;

	
	
	/**
	 * label identifiant du compte 
	 */
	private JLabel labelIdentifiant;

	/**
	 * label fonction
	 */
	private JLabel labelFonction;


	private JLabel labelRecherche;
	
	
	

	/**
	 * bouton d'ajout du compte 
	 */
	private JButton boutonAjouter;
	
	private JButton boutonRecherche;

	/**
	 * bouton qui permet d'afficher tous les compte 
	 */
	private JButton boutonAffichageTousLesComptes ;

	/**
	 * Zone de texte pour afficher les compte 
	 */
	private JTextArea zoneTextListCompte;
	
	private JTextArea zoneTextCompte;

	/**
	 * Zone de défilement pour la zone de texte
	 */
	private JScrollPane zoneDefilement;

	
	
	/**
	 * instance de CompteDAO permettant les accès à la base de données
	 */
	private CompteDAO monCompteDAO;

	/**
	 * Constructeur Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public CompteGUI() {
		// on instancie la classe Client DAO
		this.monCompteDAO = new CompteDAO ();
		

		// on fixe le titre de la fenêtre
		this.setTitle("Compte");
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);

		// création du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gérer la position des éléments
		// il autorisera un retaillage de la fenêtre en conservant la
		// présentation
		// BoxLayout permet par exemple de positionner les élements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldIdentifiant = new JTextField();
		textFieldFonction = new JTextField();
		textFieldRecherche = new JTextField();
		
		boutonAjouter = new JButton("Ajouter");
		boutonRecherche = new JButton ("Rechercher");
		boutonAffichageTousLesComptes = new JButton(
				"Afficher tous les comptes");
		labelIdentifiant = new JLabel("Identifiant compte :");
		labelFonction = new JLabel("Fonction :");
		labelRecherche = new JLabel ("Rechercher :");

		zoneTextCompte = new JTextArea(10, 20);
		zoneTextCompte.setEditable(false);
		
		zoneTextListCompte = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListCompte);
		zoneTextListCompte.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelIdentifiant);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldIdentifiant);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelFonction);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldFonction);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

	
		containerPanel.add(boutonAjouter);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(labelRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(boutonRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(textFieldRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(zoneTextClient);
		

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageTousLesComptes);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des écouteurs sur les boutons pour gérer les évènements
		boutonAjouter.addActionListener(this);
		boutonAffichageTousLesComptes.addActionListener(this);

		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenêtre
		this.setVisible(true);
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on crée l'objet message
				Compte a = new Compte(
						Double.parseDouble(this.textFieldIdentifiant.getText()),
						this.textFieldFonction.getText());
						
				// on demande à la classe de communication d'envoyer le client
				// dans la table Client
				retour = monCompteDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutée ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "Compte ajouté !");
				else
					JOptionPane.showMessageDialog(this, "Erreur ajout compte",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageTousLesComptes) {
				// on demande à la classe CompteDAO d'ajouter le message
				// dans la base de données
				List<Compte> liste = monCompteDAO.getListeCompte();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListCompte.setText("");
				// on affiche dans la console du client les client reçus
				for (Compte a : liste) {
					zoneTextListCompte.append(a.toString());
					zoneTextListCompte.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			} else if (ae.getSource() == boutonRecherche){
				
				zoneTextCompte.append(monCompteDAO.getCompte((this.textFieldRecherche.getText())).toString());
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contrôler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contrôler vos saisies");
		}

	}

	public static void main(String[] args) {
		new CompteGUI ();
	}

}