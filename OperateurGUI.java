

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe OperateurGUI
 * Définit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau Operateur dans la table Operateur via
 * la saisie des identifiants des Operateurs, son nom, son prénom, son numero de telephone,
 * et la date de mise en service de l'operateur , 
 *    - Permet l'affichage de tous les Operateurs dans la console
 *    
 * @author Chokote & Leondaris
 * @version 1.3
 * */


public class OperateurGUI extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * OperateurGUI
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant Operateur
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour le champ nom operateur
	 */
	private JTextField textFieldNom;

	/**
	 * zone de texte pour le prénom
	 * 
	 */
	private JTextField textFieldPrenom;
	
	/**
	 * zone de texte pour le numero de tel
	 * 
	 */
	private JTextField textFieldNumTelephone;
	/**
	 * zone de texte pour la date de mise en service
	 * 
	 */
	private JTextField textFieldDateMes;
	
	
	private JTextField textFieldRechercheNom;
	private JTextField textFieldRecherchePrenom;
	
	
	/**
	 * label identifiant de l'operateur 
	 */
	private JLabel labelIdentifiant;

	/**
	 * label nom de l'operateur 
	 */
	private JLabel labelNom;

	/**
	 * label prénom de l'operateur 
	 */
	private JLabel labelPrenom;

	/**
	 * label numero de telephone
	 */
	private JLabel labelNumTelephone;
	
	/**
	 * label date de mise en service
	 */
	private JLabel labelDateMes;
	
	private JLabel labelRechercheNom;
	private JLabel labelRecherchePrenom;
	
	
	
	

	/**
	 * bouton d'ajout de l'operateur
	 */
	private JButton boutonAjouter;
	
	private JButton boutonRecherche;

	/**
	 * bouton qui permet d'afficher tous les operateurs
	 */
	private JButton boutonAffichageTousLesOperateurs;

	/**
	 * Zone de texte pour afficher les operateurs
	 */
	private JTextArea zoneTextListOperateur;
	
	private JTextArea zoneTextOperateur;

	/**
	 * Zone de défilement pour la zone de texte
	 */
	private JScrollPane zoneDefilement;

	
	
	/**
	 * instance de OperateurDAO permettant les accès à la base de données
	 */
	private OperateurDAO monOperateurDAO;

	/**
	 * Constructeur Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public OperateurGUI() {
		// on instancie la classe Operateur DAO
		this.monOperateurDAO = new OperateurDAO ();
		

		// on fixe le titre de la fenêtre
		this.setTitle("Operateur");
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);

		// création du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gérer la position des éléments
		// il autorisera un retaillage de la fenêtre en conservant la
		// présentation
		// BoxLayout permet par exemple de positionner les élements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldIdentifiant = new JTextField();
		textFieldNom = new JTextField();
		textFieldPrenom = new JTextField();
		textFieldNumTelephone = new JTextField();
		textFieldDateMes = new JTextField();
		textFieldRechercheNom = new JTextField();
		textFieldRecherchePrenom = new JTextField();
		
		boutonAjouter = new JButton("Ajouter");
		boutonRecherche = new JButton ("Rechercher");
		boutonAffichageTousLesOperateurs = new JButton(
				"Afficher tous les Operateurs");
		labelIdentifiant = new JLabel("Identifiant Operateur :");
		labelNom = new JLabel("Nom Operateur :");
		labelPrenom = new JLabel("Prenom Operateur :");
		labelNumTelephone = new JLabel("Numéro Telephone :");
		labelDateMes = new JLabel("Date de mise en service :");
		labelRechercheNom = new JLabel ("Rechercher nom :");
		labelRecherchePrenom = new JLabel ("Rechercher nom :");

		zoneTextOperateur = new JTextArea(10, 20);
		zoneTextOperateur.setEditable(false);
		
		zoneTextListOperateur = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListOperateur);
		zoneTextListOperateur.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelIdentifiant);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldIdentifiant);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelNom);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNom);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelPrenom);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldPrenom);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNumTelephone);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNumTelephone);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDateMes);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDateMes);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		containerPanel.add(boutonAjouter);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(labelRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(boutonRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(textFieldRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(zoneTextClient);
		

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageTousLesOperateurs);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des écouteurs sur les boutons pour gérer les évènements
		boutonAjouter.addActionListener(this);
		boutonAffichageTousLesOperateurs.addActionListener(this);

		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenêtre
		this.setVisible(true);
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on crée l'objet message
				Operateur a = new Operateur(
						Double.parseDouble(this.textFieldIdentifiant.getText()),
						this.textFieldNom.getText(),
						this.textFieldPrenom.getText(),
						Long.parseLong(this.textFieldNumTelephone.getText()),
						Date.valueOf(this.textFieldDateMes.getText())); 
				
				// on demande à la classe de communication d'envoyer le client
				// dans la table Client
				retour = monOperateurDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutée ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "Operateur ajouté !");
				else
					JOptionPane.showMessageDialog(this, "Erreur ajout Operateur",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageTousLesOperateurs) {
				// on demande à la classe OperateurDAO d'ajouter le message
				// dans la base de données
				List<Operateur> liste = monOperateurDAO.getListeOperateur();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListOperateur.setText("");
				// on affiche dans la console du client les client reçus
				for (Operateur a : liste) {
					zoneTextListOperateur.append(a.toString());
					zoneTextListOperateur.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			} else if (ae.getSource() == boutonRecherche){
				
				zoneTextOperateur.append(monOperateurDAO.getOperateur(this.textFieldRechercheNom.getText(), this.textFieldRecherchePrenom.getText()).toString());
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contrôler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contrôler vos saisies");
		}

	}

	public static void main(String[] args) {
		new OperateurGUI ();
	}

}