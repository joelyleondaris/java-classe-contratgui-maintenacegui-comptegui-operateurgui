

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe MaintenanceFenetre
 * Définit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'une nouvelle Maintenance dans la table Maintenance via
 * la saisie des identifiants de la Maintenance, son statut, son type, sa date
 * le client concerné par la Maintenance, l'operateur qui doit s'occuper de la maintenance
 *    - Permet l'affichage de toutes les Maintenances dans la console
 *    
 * @author Chokote & Leondaris
 * @version 1.3
 * */


public class MaintenanceGUI extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class MaintenanceGUI does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * MaintenanceFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant de la Maintenance
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour le champ statut
	 */
	private JTextField textFieldStatut;

	/**
	 * zone de texte pour le type de la Maintenance
	 */
	private JTextField textFieldType;
	/**
	 * zone de texte pour la date de la Maintenance
	 * 
	 */
	private JTextField textFieldDate;
	/**
	 * zone de texte pour le client
	 * 
	 */
	private JTextField textFieldClient;
	/**
	 * zone de texte pour l'operateur qui s'occuper de la maintenance
	 * 
	 */
	private JTextField textFieldOperateur;
	
	
	private JTextField textFieldRecherche;

	
	
	/**
	 * label identifiant de la maintenance
	 */
	private JLabel labelIdentifiant;

	/**
	 * label statut de la maintenance
	 */
	private JLabel labelStatut;

	/**
	 * label type
	 */
	private JLabel labelType;

	/**
	 * label date
	 */
	private JLabel labelDate;
	
	/**
	 * label client
	 */
	private JLabel labelClient;
	
	/**
	 * label opérateur
	 */
	private JLabel labelOperateur;
	
	
	private JLabel labelRecherche;
	
	
	

	/**
	 * bouton d'ajout d'une maintenance
	 */
	private JButton boutonAjouter;
	
	private JButton boutonRecherche;

	/**
	 * bouton qui permet d'afficher toutes les maintenances
	 */
	private JButton boutonAffichageToutesLesMaintenances;

	/**
	 * Zone de texte pour afficher les maintenances
	 */
	private JTextArea zoneTextListMaintenance;
	
	private JTextArea zoneTextMaintenance;

	/**
	 * Zone de défilement pour la zone de texte
	 */
	private JScrollPane zoneDefilement;

	
	
	/**
	 * instance de MaintenanceDAO permettant les accès à la base de données
	 */
	private MaintenanceDAO monMaintenanceDAO;

	/**
	 * Constructeur Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public MaintenanceGUI() {
		// on instancie la classe Maintenance DAO
		this.monMaintenanceDAO = new MaintenanceDAO ();
		

		// on fixe le titre de la fenêtre
		this.setTitle("Maintenance");
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);

		// création du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gérer la position des éléments
		// il autorisera un retaillage de la fenêtre en conservant la
		// présentation
		// BoxLayout permet par exemple de positionner les élements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldIdentifiant = new JTextField();
		textFieldStatut = new JTextField();
		textFieldType = new JTextField();
		textFieldDate = new JTextField();
		textFieldClient = new JTextField();
		textFieldOperateur = new JTextField();
		textFieldRecherche = new JTextField();
		
		boutonAjouter = new JButton("Ajouter");
		boutonRecherche = new JButton ("Rechercher");
		boutonAffichageToutesLesMaintenances = new JButton(
				"Afficher tous les Maintenances");
		labelIdentifiant = new JLabel("Identifiant Maintenance :");
		labelStatut = new JLabel("Statut de la Maintenance :");
		labelType = new JLabel("Type de la Maintenance:");
		labelDate = new JLabel("Date de la Maintenance :");
		labelClient = new JLabel("Client :");
		labelOperateur = new JLabel("Operateur :");
		labelRecherche = new JLabel ("Rechercher :");

		zoneTextMaintenance = new JTextArea(10, 20);
		zoneTextMaintenance.setEditable(false);
		
		zoneTextListMaintenance = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListMaintenance);
		zoneTextListMaintenance.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelIdentifiant);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldIdentifiant);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelStatut);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldStatut);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelType);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldType);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDate);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDate);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelClient);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldClient);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(labelOperateur);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldOperateur);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		
		containerPanel.add(boutonAjouter);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(labelRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(boutonRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(textFieldRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(zoneTextClient);
		

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageToutesLesMaintenances);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des écouteurs sur les boutons pour gérer les évènements
		boutonAjouter.addActionListener(this);
		boutonAffichageToutesLesMaintenances.addActionListener(this);

		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenêtre
		this.setVisible(true);
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				ClientDAO clientDAO = new ClientDAO();
				OperateurDAO operateurDAO = new OperateurDAO();
				// on crée l'objet message
				Maintenance a = new Maintenance(
						Double.parseDouble(this.textFieldIdentifiant.getText()),
						this.textFieldStatut.getText(),
						this.textFieldType.getText(),
						Date.valueOf(this.textFieldDate.getText()),
						clientDAO.getClient(this.textFieldClient.getText()),
						operateurDAO.getOperateur(Double.parseDouble(this.textFieldOperateur.getText())));
						
						
				// on demande à la classe de communication d'envoyer la Maintenance
				// dans la table Maintenance
				retour = monMaintenanceDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutée ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "Maintenance ajoutée !");
				else
					JOptionPane.showMessageDialog(this, "Erreur ajout Maintenance",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageToutesLesMaintenances) {
				// on demande à la classe MaintenanceDAO d'ajouter le message
				// dans la base de données
				List<Maintenance> liste = monMaintenanceDAO.getListeMaintenance();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListMaintenance.setText("");
				// on affiche dans la console de la Maintenance les Maintenances reçus
				for (Maintenance a : liste) {
					zoneTextListMaintenance.append(a.toString());
					zoneTextListMaintenance.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			} else if (ae.getSource() == boutonRecherche){
				
				zoneTextMaintenance.append(monMaintenanceDAO.getMaintenance(Long.parseLong(this.textFieldRecherche.getText())).toString());
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contrôler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contrôler vos saisies");
		}

	}

	public static void main(String[] args) {
		new MaintenanceGUI ();
	}

}